package ictgradschool.industry.lab06.ex01;

/**
 * Should represent a Sphere.
 *
 * TODO Implement this class.
 */
public class Sphere {
    private double radius;
    private double[] centroidCoordinates =new double [3];
    private static int count;

    public Sphere( double radius, double[] coordinates)
    {
        this.radius = radius;
        this.centroidCoordinates= coordinates;
        this.count++;
    }

    public int getCount(){ return count;}
    public double getSurfaceArea() { return 4*Math.PI*Math.pow(radius, 2);}
    public double getVolume() { return 4*Math.PI*Math.pow(radius, 3)/3;}


}
